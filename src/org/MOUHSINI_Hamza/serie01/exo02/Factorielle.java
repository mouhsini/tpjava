package org.MOUHSINI_Hamza.serie01.exo02;

import java.math.BigInteger;


public class Factorielle {

	public int intFactorielle(int n) 
	{
		int i , NB=1;
		for(i=1;i<n+1;i++) {
			NB=NB*i;
		}
			return NB;
	}
	
	public double doubleFactorielle(double n) 
	{
		double i , NB=1;
		for(i=1;i<n+1;i++) {
			NB=NB*i;
		}
			return NB;
	}

 public BigInteger bigIntFactorielle(int n) 
	{
	 BigInteger f = new BigInteger("1"); 
	  

     for (int i = 1; i < n+1; i++) 
         f = f.multiply(BigInteger.valueOf(i)); 

     return f; 
	}
	
}
