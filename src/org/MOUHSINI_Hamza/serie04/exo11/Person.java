package org.MOUHSINI_Hamza.serie04.exo11;

public class Person 
{
private String firstName;
private String LastName;
private int age;


public void setLastName(String lastName) {
	LastName = lastName;
}
public Person(String firstName, String lastName, int age) {
	super();
	this.firstName = firstName;
	this.LastName = lastName;
	this.age = age;
}
public Person() {
	super();
}
public String setLastName() {
	return LastName;
}
public String getLastName() {
	return LastName;
}
public void setFirstName(String firstName) {
	this.firstName = firstName;
}
public void setAge(int age) {
	this.age = age;
}
public String getFirstName() {
	return firstName;
}
public int getAge() {
	return age;
}

@Override
public String toString() {
	
	return "Person [LastName= "+LastName+" firstName= "+firstName+" age= "+age;
}


}
