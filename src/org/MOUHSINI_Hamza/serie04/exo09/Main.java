package org.MOUHSINI_Hamza.serie04.exo09;

import java.util.function.Predicate;


public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Predicate<String> chaine =  s -> s.length() > 4;
		System.out.println(chaine.test("tests"));
		
		Predicate<String> chaine_non_vide =  s -> s.length() !=0;
		System.out.println(chaine_non_vide.test(""));
	
	
		
		Predicate<String> chaine_commence_J =  s -> s.charAt(0)=='J'; 
		System.out.println(chaine_commence_J.test("Jemremiez"));

		Predicate<String> chaine_5_caractere =  s -> s.length()==5; 
		System.out.println(chaine_5_caractere.test("Jemremiez"));
		
		Predicate<String> chaine_5_caractere_Commence_par_J =  chaine_commence_J.and(chaine_5_caractere) ; 
		System.out.println(chaine_5_caractere_Commence_par_J.test("Jemre"));
		
	}

}
