package org.MOUHSINI_Hamza.serie05.exo13;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.OptionalLong;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;




public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Function<String,Integer> chaineToLength = s -> s==null ? 0 : s.length();

		Comparator<String> comparerDeuxChaine = Comparator.comparing(chaineToLength);
		Comparator<String> comparerDeuxChaine2 = Comparator.comparing((String s) -> s);
		Comparator<String> compare_longchaine_thenchaine = comparerDeuxChaine.thenComparing(comparerDeuxChaine2);
		
		
		 List<String> words= new ArrayList<>();


		words.add("one");	
		words.add("two");	
		words.add("three");	
		words.add("four");	
		words.add("five");	
		words.add("six");	
		words.add("seven");	
		words.add("eight");	
		words.add("nine");	
		words.add("ten");	
		words.add("eleven");	
		words.add("twelve");
		
		
		words.forEach((value) -> 
		{System.out.println(value);});
		
		 List<String> words_3= new ArrayList<>();
		
			words.forEach((value) ->
			{
				if(value.length() ==3 ){
				words_3.add(value);	
			}				
			});
			System.out.println("");
			
			words_3.forEach((value) -> 
			{System.out.println(value);});
			
			System.out.println("");

			words.sort(Comparator.nullsLast(comparerDeuxChaine));
			
			words.forEach((value) -> 
			{System.out.println(value);});
			
			System.out.println("");
			words.sort(Comparator.nullsLast(compare_longchaine_thenchaine));

			words.forEach((value) -> 
			{System.out.println(value);});
			

			LongAdder somme = new LongAdder();
			LongAdder count = new LongAdder();
			words.forEach(valeur -> {

		
			somme.add(valeur.length());
		count.add(1);
			
			});
			
	      double moy=(somme.longValue())/count.longValue();
		System.out.println("\n"+moy);
	
		Function <String, Stream<String>> flatMapper= str-> str.chars()
				.mapToObj(s->(char)s).map(c->Character.toString(c));
	
List<String> letters = words.stream()
          .flatMap(flatMapper)
          .collect(Collectors.toList());

System.out.println("la somme des longueur :"+letters.size());

Integer letter3 = (int) letters.stream()

.flatMap(flatMapper)
.distinct()
.count();

System.out.println("la somme des longueur sans doublon :"+letter3);



OptionalLong letters1 = letters.stream()

.mapToLong((String str) -> (long) str.charAt(0))
.distinct()
.min();

char min = (char) letters1.getAsLong();
System.out.println("caratere minimal :"+min+"\n");

OptionalLong letters2 = letters.stream()

.mapToLong((String str) -> (long) str.charAt(0))
.distinct()
.max();

char max = (char) letters2.getAsLong();
System.out.println("caratere maximal :"+max);

	
	}

}
