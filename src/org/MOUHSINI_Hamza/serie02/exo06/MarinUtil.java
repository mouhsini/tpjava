package org.MOUHSINI_Hamza.serie02.exo06;
import org.MOUHSINI_Hamza.serie02.exo05.Marin;
import java.util.Arrays;
public class MarinUtil {
	
	public void augmenteSalaire(Marin [] marins, int pourcentage)
	{
		for(int i =0;i<marins.length;i++)
		{
			marins[i].setSalaire(marins[i].getSalaire() + pourcentage);
		}
	}
	
	public double getMaxSalaire(Marin[] marins)
	{
	
		double tab_Salaire[]= new double[marins.length];
		for(int i=0;i<marins.length;i++)
		{
			tab_Salaire[i]=marins[i].getSalaire();
		}
 	    Arrays.sort(tab_Salaire);
	
	return  tab_Salaire[tab_Salaire.length-1];
	}

	
	public double getMoyenneSalaire(Marin[] marins)
	{
		double sum=0;
		
		for(int i=0;i<marins.length;i++)
		{
	
		sum= (sum + marins[i].getSalaire());
		
		}
		return (sum/marins.length);		
	    
	}
	
	public double getMedianeSalaire(Marin[] marins)
	{
	double tab_salaire[]=new double[marins.length];
	
	for(int i=0;i<marins.length;i++)
	{
		tab_salaire[i]=marins[i].getSalaire();
	}
	 Arrays.sort(tab_salaire);
	 
	 if(tab_salaire.length%2!=0)
	
		 return tab_salaire[(tab_salaire.length)/2];
	 else
	return (tab_salaire[tab_salaire.length/2-1] + tab_salaire[tab_salaire.length/2])/2;
	 
	}
	

}
