package org.MOUHSINI_Hamza.serie02.exo05;

public class Marin {
	private String nom,prenom;
	double salaire;
	public Marin(String nom, String prenom, double salaire) {
    super();
	this.nom = nom;
	this.prenom = prenom;
	this.salaire = salaire;
	}
	public Marin(String nom, double salaire) {
	super();
	this.nom = nom;
	this.prenom="";
	this.salaire = salaire;
	}
	public String getNom() {
	return nom;
	}
	public void setNom(String nom) {
	this.nom = nom;
	}
	public String getPrenom() {
	return prenom;
	}
	public void setPrenom(String prenom) {
	this.prenom = prenom;
	}
	public double getSalaire() {
	return salaire;
	}
	public void setSalaire(double salaire) {
	this.salaire = salaire;
	}
	public  double augmentation( int ag)
	{
	return this.salaire + ag;
	}
	@Override
	public String toString() {
	return "Marin [nom=" + nom + ", prenom=" + prenom + ", salaire=" + salaire + "]";
	}
	@Override
	public int hashCode() {
	int hashCode  = 17;
	hashCode  = 31 * hashCode  + ((nom == null) ? 0 : nom.hashCode());
	hashCode  = 31 * hashCode  + ((prenom == null) ? 0 : prenom.hashCode());
	long Salaire;
	Salaire = Double.doubleToLongBits(salaire);
	hashCode  = 31 * hashCode  + (int) (Salaire) ;
	return hashCode ;
	}
	@Override
	public boolean equals(Object m) {

	Marin other = (Marin) m;

if (!nom.equals(other.nom))
	return false;

if (!prenom.equals(other.prenom))
	return false;
 
if (Double.doubleToLongBits(salaire) != Double.doubleToLongBits(other.salaire))
	return false;
	return true;
	}
	public int compareTo(Marin marin1) {
		if(this.nom.equals(marin1.nom))
			return this.nom.compareTo(marin1.nom);
		else
			return this.nom.compareTo(marin1.nom);
	}
	
}
