package org.MOUHSINI_Hamza.serie03.exo07;
import org.MOUHSINI_Hamza.serie02.exo05.Marin;
import java.util.ArrayList;

public class Equipage {
	 private int nombre_max=4;
	private ArrayList<Marin> equipage= new ArrayList<Marin>();
	

	public boolean addMarin(Marin marin)
	{
		if(equipage.size()<this.nombre_max)
			return equipage.add(marin);
		   else
		return false;

	}
	
	public boolean removeMarin(Marin marin)
	{
		return equipage.remove(marin);
	}
	
	
	public boolean isMarinPresent(Marin marin)
	{
		return equipage.contains(marin);
		
	}

	public void addAllEquipage(Equipage equipe)
	{
		Marin marin = new Marin ("","",0);
		for(int i=0;i<equipe.equipage.size();i++)
		{
			marin=equipe.equipage.get(i);
			if(!this.equipage.contains(marin))
				
				this.equipage.add(marin);
			
		}
		
	}
	
	public void clear(Equipage equipe)
	{
		
		for(int i=0;i<equipe.equipage.size();i++)
		{
			removeMarin(equipe.equipage.get(i));
		}
	}
	
	public int getNombreMarins(Equipage equipe)
	{
		
		return equipe.equipage.size();
	}
	
	
	public double getMoyenneSalaire(Equipage equipe)
	{
		double mean=0;
		for(int i=0;i<equipe.equipage.size();i++)
		{
			mean= mean+equipe.equipage.get(i).getSalaire();
		}
		return mean/getNombreMarins(equipe);
	}


	
	public int getNombre_max() {
		return nombre_max;
	}

	public void setNombre_max(int nombre_max) {
		this.nombre_max = nombre_max;
	}


	@Override
	public String toString() {
		return "EquipageLimite [nombre_max=" + nombre_max + ", equipage=" + equipage + "]";
	}
	
	

}
